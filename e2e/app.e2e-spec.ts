import { IdeaConfiguratorPage } from './app.po';

describe('idea-configurator App', () => {
  let page: IdeaConfiguratorPage;

  beforeEach(() => {
    page = new IdeaConfiguratorPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
