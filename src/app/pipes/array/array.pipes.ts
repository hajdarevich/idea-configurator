import { Pipe, PipeTransform } from '@angular/core';


/**
 * Pipe to transform array of string to array of objects with labels and values that is used for ng-select dropdown
 * fields.
 */
@Pipe({
  name: 'beLabelValueDropdown'
})
export class ArrayLabelValueDropdown implements PipeTransform {

  transform(array: Array<string>): Array<{}> {

    array = array || [];

    return array.map((value: string) => ({
      label: value,
      value: value
    }));
  }
}
