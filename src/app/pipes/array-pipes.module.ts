import { NgModule } from '@angular/core';
import { ArrayLabelValueDropdown } from './array/array.pipes';


@NgModule({
  declarations: [
    ArrayLabelValueDropdown
  ],
  exports: [
    ArrayLabelValueDropdown
  ],
})
export class ArrayPipesModule {
}
