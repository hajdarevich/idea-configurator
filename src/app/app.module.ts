import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MockBackend } from '@angular/http/testing';
import { BaseRequestOptions, HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { BsDropdownModule, ModalModule } from 'ngx-bootstrap';
import { SelectModule } from 'ng-select';
import { DragulaModule, DragulaService } from 'ng2-dragula';

import { AppComponent } from './app.component';
import { AuthGuard } from './services/guards/auth.guard';
import { AlertService } from './services/alert.service';
import { AuthenticationService } from './services/auth.service';
import { UserService } from './services/user.service';
import { fakeBackendProvider } from './helpers/fake-backend';
import { routing } from './app.routing';

import { HomeComponent } from './modules/home/home.component';
import { LoginComponent } from './_syscomponents/login.component';
import { HeaderComponent } from './modules/partials/header.component';
import { AlertComponent } from './directives/alert.component';
import { storeProvider, effects, instrumentation } from './store';
import { SharedModule } from './modules/shared/shared.module';
import { ArrayPipesModule } from './pipes/array-pipes.module';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    HeaderComponent,
    AlertComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    DragulaModule,
    routing,
    storeProvider,
    effects,
    instrumentation,
    SelectModule,
    BsDropdownModule.forRoot(),
    SharedModule.forRoot(),
    ModalModule.forRoot(),
    ArrayPipesModule,
  ],
  providers: [
    AuthGuard,
    AlertService,
    AuthenticationService,
    UserService,
    DragulaService,
    // providers used to create fake backend
    fakeBackendProvider,
    MockBackend,
    BaseRequestOptions,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
