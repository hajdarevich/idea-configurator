import { ModuleWithProviders, NgModule } from '@angular/core';
import { combineReducers, StoreModule, ActionReducer, Action } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { CommonModule } from '@angular/common';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { environment } from '../../environments/environment';
import { authReducer } from './auth/auth.reducer';
import { AuthEffects } from './auth/auth.effects';
import { configuratorReducer } from './configurator/configurator.reducer';
import { ConfiguratorEffects } from './configurator/configurator.effects';



export interface IAppState {
  auth: {};
  configurator: {};
}

/**
 * List of all Action Reducers
 *
 * @type {ActionReducer<IAppState>}
 */
export const reducers: IAppState = {
  auth: authReducer,
  configurator: configuratorReducer
};

const productionReducer: ActionReducer<IAppState> = combineReducers(reducers);

export function reducer(state: IAppState, action: Action) {
  return productionReducer(state, action);
}

/**
 * Export all stores with it"s reducers, this constant must be used in AppModule
 *
 * @type {ModuleWithProviders}
 */
export const storeProvider: ModuleWithProviders = StoreModule.provideStore(reducer);

/**
 * @TOOD this should be removed when we re-enable AOT again.
 */
@NgModule()
export class DummyModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CommonModule
    };
  }
}

/**
 * If we are in development mode, add instrumentation
 */
export const instrumentation: ModuleWithProviders =
  (!environment.production) ? StoreDevtoolsModule.instrumentOnlyWithExtension() : DummyModule.forRoot();

/**
 * Export all effects needed for application bootstrapping.
 *
 * @type ModuleWithProviders[]
 */
export const effects: ModuleWithProviders[] = [
  EffectsModule.run(AuthEffects),
  EffectsModule.run(ConfiguratorEffects),
];
