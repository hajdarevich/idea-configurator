import { Action, ActionReducer } from '@ngrx/store';
import { LOGIN_SUCCESS, LOGOUT_SUCCESS } from './auth.actions';

export interface ILoginState {
  authorized: boolean;
}

const initialState: ILoginState = {
  authorized: false
};

export const authReducer: ActionReducer<{}> = (state: ILoginState = initialState, action: Action): {} => {

  switch (action.type) {

    case LOGIN_SUCCESS:

      return Object.assign({}, state, {
        authorized: action.payload
      });

    case LOGOUT_SUCCESS:

      return Object.assign({}, state, {
        authorized: action.payload
      });


    default:
      return state;
  }

};
