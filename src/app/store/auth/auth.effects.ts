import { Effect, Actions } from '@ngrx/effects';
import { Injectable } from '@angular/core';

import {
    LOGIN, LOGIN_SUCCESS, LOGOUT_SUCCESS, LOGOUT
} from './auth.actions';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/auth.service';

@Injectable()
export class AuthEffects {

  /**
   * Login user to IDEA
   *
   * @type Observable<{}>
   */
  @Effect() loginUser$: Observable<Action> = this.actions$
    .ofType(LOGIN)
    .map(() => {
        return ({ payload: true, type: LOGIN_SUCCESS });
    });

    /**
     * Logout user from IDEA
     *
     * @type Observable<{}>
     */
    @Effect() logoutUser$: Observable<Action> = this.actions$
        .ofType(LOGOUT)
        .map(() => {
            return ({ payload: true, type: LOGOUT_SUCCESS });
        });


    //
  constructor(private actions$: Actions,
              private router: Router,
              private authenticationService: AuthenticationService) {
  }
}
