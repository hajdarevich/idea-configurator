import { Action, ActionReducer } from '@ngrx/store';
import {
  UNIT_TYPES_GET_SUCCESS, CONFIGURATOR_LOADING, REFRIGERANTS_GET_SUCCESS,
  CIRCUITS_GET_SUCCESS, CATEGORY_LIST_ITEMS_GET_SUCCESS, CATEGORY_LIST_ITEMS_GET,
  CATEGORY_BRAND_ITEMS_GET, CATEGORY_BRAND_ITEMS_GET_SUCCESS, CATEGORY_TYPE_ITEMS_GET_SUCCESS, CATEGORY_TYPE_ITEMS_GET,
  FAN_BRAND_ITEMS_GET, FAN_BRAND_ITEMS_GET_SUCCESS, FAN_TYPE_ITEMS_GET, FAN_TYPE_ITEMS_GET_SUCCESS, FAN_LIST_ITEMS_GET,
  FAN_LIST_ITEMS_GET_SUCCESS
} from './configurator.actions';

export interface selectedOptions {
  type: string;
  value: string;
}

interface ICategoryItems {
  loading: boolean;
  items: Array<string | {}>;
  brands: Array<string | {}>;
  types: Array<string | {}>;
}

const defaultCategoryItemsState: ICategoryItems = {
  loading: false,
  items: [],
  brands: [],
  types: []
};

export interface IConfiguratorState {
  loading?: boolean;
  refrigerants?: string[];
  unitTypes?: string[];
  circuits?: string[];
  compressor?: ICategoryItems;
  evaporator?: ICategoryItems;
  condenser?: ICategoryItems;
  desuperheater?: ICategoryItems;
  heatRecovery?: ICategoryItems;
  fans?: ICategoryItems;
}

const initialState: IConfiguratorState = {
  loading: false,
  unitTypes: [],
  refrigerants: [],
  circuits: [],
  compressor: defaultCategoryItemsState,
  evaporator: defaultCategoryItemsState,
  condenser: defaultCategoryItemsState,
  desuperheater: defaultCategoryItemsState,
  heatRecovery: defaultCategoryItemsState,
  fans: defaultCategoryItemsState
};

export const configuratorReducer: ActionReducer<{}> = (state: IConfiguratorState = initialState, action: Action): {} => {

  switch (action.type) {

    case UNIT_TYPES_GET_SUCCESS:

      return Object.assign({}, state, {
        unitTypes: [...action.payload]
      });

    case REFRIGERANTS_GET_SUCCESS:

      return Object.assign({}, state, {
        refrigerants: [...action.payload]
      });

    case CIRCUITS_GET_SUCCESS:

      return Object.assign({}, state, {
        circuits: [...action.payload]
      });

    case CONFIGURATOR_LOADING:

      return Object.assign({}, state, {
        loading: action.payload
      });

    case CATEGORY_LIST_ITEMS_GET:

      const newCategoryState: IConfiguratorState = Object.assign({}, state);
      newCategoryState[action.payload.category] = Object.assign({}, newCategoryState[action.payload.category], {
        loading: true,
        items: []
      });
      return newCategoryState;

    case CATEGORY_LIST_ITEMS_GET_SUCCESS:

      const newState: IConfiguratorState = Object.assign({}, state);

      newState[action.payload.category] = Object.assign({}, newState[action.payload.category], {
        loading: false,
        items: action.payload.items
      });
      return newState;

    case FAN_BRAND_ITEMS_GET: {

      const newBrandsState: IConfiguratorState = Object.assign({}, state);
      newBrandsState['fans'] = Object.assign({}, newBrandsState['fans'], {
        loading: true,
        items: []
      });
      return newBrandsState;
    }

    case FAN_BRAND_ITEMS_GET_SUCCESS: {

      const newBrandsState: IConfiguratorState = Object.assign({}, state);

      newBrandsState['fans'] = Object.assign({}, newBrandsState['fans'], {
        loading: false,
        brands: action.payload.items
      });
      return newBrandsState;
    }

    case FAN_TYPE_ITEMS_GET: {

      const newBrandsState: IConfiguratorState = Object.assign({}, state);
      newBrandsState['fans'] = Object.assign({}, newBrandsState['fans'], {
        loading: true,
        types: []
      });
      return newBrandsState;
    }

    case FAN_TYPE_ITEMS_GET_SUCCESS: {

      const newBrandsState: IConfiguratorState = Object.assign({}, state);

      newBrandsState['fans'] = Object.assign({}, newBrandsState['fans'], {
        loading: false,
        types: action.payload.items
      });
      return newBrandsState;
    }

    case FAN_LIST_ITEMS_GET: {

      const newBrandsState: IConfiguratorState = Object.assign({}, state);
      newBrandsState['fans'] = Object.assign({}, newBrandsState['fans'], {
        loading: true,
        items: []
      });
      return newBrandsState;
    }

    case FAN_LIST_ITEMS_GET_SUCCESS: {

      const newBrandsState: IConfiguratorState = Object.assign({}, state);

      newBrandsState['fans'] = Object.assign({}, newBrandsState['fans'], {
        loading: false,
        items: action.payload.items
      });
      return newBrandsState;
    }

    case CATEGORY_BRAND_ITEMS_GET: {

      const newBrandsState: IConfiguratorState = Object.assign({}, state);
      newBrandsState[action.payload.category] = Object.assign({}, newBrandsState[action.payload.category], {
        loading: true,
        items: []
      });
      return newBrandsState;
    }

    case CATEGORY_BRAND_ITEMS_GET_SUCCESS:

      const newBrandsState: IConfiguratorState = Object.assign({}, state);

      newBrandsState[action.payload.category] = Object.assign({}, newBrandsState[action.payload.category], {
        loading: false,
        brands: action.payload.items
      });
      return newBrandsState;

    case CATEGORY_TYPE_ITEMS_GET: {

      const newTypesState: IConfiguratorState = Object.assign({}, state);
      newTypesState[action.payload.category] = Object.assign({}, newTypesState[action.payload.category], {
        loading: true,
        types: []
      });
      return newTypesState;
    }

    case CATEGORY_TYPE_ITEMS_GET_SUCCESS:

      const newTypesState: IConfiguratorState = Object.assign({}, state);
      newTypesState[action.payload.category] = Object.assign({}, newTypesState[action.payload.category], {
        loading: false,
        types: action.payload.items
      });
      return newTypesState;

    default:
      return state;
  }

};
