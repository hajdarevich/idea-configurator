import { Effect, Actions } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import { Action } from '@ngrx/store';

import {
  UNIT_TYPES_GET, UNIT_TYPES_GET_SUCCESS, CONFIGURATOR_LOADING, REFRIGERANTS_GET,
  REFRIGERANTS_GET_SUCCESS, CIRCUITS_GET, CIRCUITS_GET_SUCCESS, CATEGORY_LIST_ITEMS_GET,
  CATEGORY_LIST_ITEMS_GET_SUCCESS, CATEGORY_BRAND_ITEMS_GET, CATEGORY_BRAND_ITEMS_GET_SUCCESS,
  CATEGORY_TYPE_ITEMS_GET, CATEGORY_TYPE_ITEMS_GET_SUCCESS, FAN_BRAND_ITEMS_GET, FAN_BRAND_ITEMS_GET_SUCCESS,
  FAN_TYPE_ITEMS_GET, FAN_LIST_ITEMS_GET, FAN_LIST_ITEMS_GET_SUCCESS, FAN_TYPE_ITEMS_GET_SUCCESS
} from './configurator.actions';
import { Http, Response } from '@angular/http';

const loadingActions: string[] = [
  UNIT_TYPES_GET,
  REFRIGERANTS_GET
];

const loadingActionsEnd: string[] = [
  UNIT_TYPES_GET_SUCCESS,
  REFRIGERANTS_GET_SUCCESS
];

@Injectable()
export class ConfiguratorEffects {

  /**
   * Effects that invoke loading on actions
   */
  @Effect() configuratorLoading$: Observable<Action> = this.actions$
    .ofType(...loadingActions)
    .map(() => ({
      payload: true,
      type: CONFIGURATOR_LOADING
    }));

  @Effect() configuratorLoadingEnd$: Observable<Action> = this.actions$
    .ofType(...loadingActionsEnd)
    .map(() => ({
      payload: false,
      type: CONFIGURATOR_LOADING
    }));

  /**
   * Get all unit types for configurator
   *
   * @type Observable<{}>
   */
  @Effect() unitTypesGet$: Observable<Action> = this.actions$
    .ofType(UNIT_TYPES_GET)
    .map(() => {
      return ({ payload: [
        'AW', 'AA', 'WA', 'WW'
      ], type: UNIT_TYPES_GET_SUCCESS });
    });

  /**
   * Get all refrigerants for configurator
   *
   * @type Observable<{}>
   */
  @Effect() refrigerantsGet$: Observable<Action> = this.actions$
    .ofType(REFRIGERANTS_GET)
    .switchMap(() => {

      return this.http.get(`/api/refrigerants`)
        .map((response: Response) => ({
          type: REFRIGERANTS_GET_SUCCESS,
          payload: response.json()
        }));
    });

    /**
     * Get all circuits for configurator
     *
     * @type Observable<{}>
     */
    @Effect() circuitsGet$: Observable<Action> = this.actions$
        .ofType(CIRCUITS_GET)
        .map(() => {
            return ({ payload: [
                '1', '2', '3', '4'
            ], type: CIRCUITS_GET_SUCCESS });
        });

  /**
   * Get items for category list
   * @type Observable<{}>
   */
  @Effect() categoryListItemsGet$: Observable<Action> = this.actions$
      .ofType(CATEGORY_LIST_ITEMS_GET)
      .switchMap((action: Action) => {

        return this.http.get(`/api/category/${action.payload.category}/brand/${action.payload.brand}/type/${action.payload.type}`)
          .map((response: Response) => ({
            type: CATEGORY_LIST_ITEMS_GET_SUCCESS,
            payload: Object.assign({}, action.payload, {
              items: response.json()
            })
          }));
      });

  /**
   * Get items for category brands
   * @type Observable<{}>
   */
  @Effect() categoryBrandItemsGet$: Observable<Action> = this.actions$
      .ofType(CATEGORY_BRAND_ITEMS_GET)
      .mergeMap((action: Action) => {

        return this.http.get(`/api/category/${action.payload.category}/brand`)
          .map((response: Response) => ({
            type: CATEGORY_BRAND_ITEMS_GET_SUCCESS,
            payload: Object.assign({}, action.payload, {
              items: response.json()
            })
          }));
      });
    /**
     * Get items for category list
     * @type Observable<{}>
     */
    @Effect() categoryListTypesGet$: Observable<Action> = this.actions$
        .ofType(CATEGORY_TYPE_ITEMS_GET)
        .switchMap((action: Action) => {
            return this.http.get(`/api/category/${action.payload.category}/type/${action.payload.brand}`)
                .map((response: Response) => ({
                    type: CATEGORY_TYPE_ITEMS_GET_SUCCESS,
                    payload: Object.assign({}, action.payload, {
                        items: response.json()
                    })
                }));
        });

  /**
   * Get items for fan brands list
   * @type {Observable<any>}
   */
  @Effect() fanBrandItemsGet$: Observable<Action> = this.actions$
    .ofType(FAN_BRAND_ITEMS_GET)
    .switchMap((action: Action) => {

      return this.http.get(`/api/category/fans/brand`)
        .map((response: Response) => ({
          type: FAN_BRAND_ITEMS_GET_SUCCESS,
          payload: Object.assign({}, action.payload, {
            items: response.json()
          })
        }));
    });

  /**
   * Get items for fan brands list
   * @type {Observable<any>}
   */
  @Effect() fanTypeItemsGet$: Observable<Action> = this.actions$
    .ofType(FAN_TYPE_ITEMS_GET)
    .switchMap((action: Action) => {

      return this.http.get(`/api/category/fans/type/${action.payload.brand}`)
        .map((response: Response) => ({
          type: FAN_TYPE_ITEMS_GET_SUCCESS,
          payload: Object.assign({}, action.payload, {
            items: response.json()
          })
        }));
    });

  /**
   * Get items for fans list
   * @type Observable<{}>
   */
  @Effect() fansListItemsGet$: Observable<Action> = this.actions$
    .ofType(FAN_LIST_ITEMS_GET)
    .switchMap((action: Action) => {

      return this.http.get(`/api/category/fans/brand/${action.payload.brand}/type/${action.payload.type}`)
        .map((response: Response) => ({
          type: FAN_LIST_ITEMS_GET_SUCCESS,
          payload: Object.assign({}, action.payload, {
            items: response.json()
          })
        }));
    });


  constructor(private actions$: Actions, private http: Http) {
  }
}
