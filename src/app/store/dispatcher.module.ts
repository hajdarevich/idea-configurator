import { NgModule } from '@angular/core';
import { Store } from '@ngrx/store';

import { IAppState } from './index';
import { AuthenticationService } from '../services/auth.service';
import { categories } from '../app.config';
import { CATEGORY_BRAND_ITEMS_GET, CATEGORY_TYPE_ITEMS_GET } from './configurator/configurator.actions';


// global variable that control
// it default set of actions are dispatched in application
let dispatched = false;

// this module is used to dispatch actions to get items for store
// such as brands and types for each category
@NgModule()
export class DispatcherModule {

  constructor(private store: Store<IAppState>, private authService: AuthenticationService) {
    // if it not dispatched
    // dispatch it
    if (!dispatched && this.authService.isAuthenticated()) {

      // store.dispatch({
      //   type: USER_GET
      // });
      console.log('Test', categories);
      categories.forEach((category: string) => {

        const payload: { category: string } = { category: category };

        this.store.dispatch({
          type: CATEGORY_BRAND_ITEMS_GET,
          payload: payload
        });
        this.store.dispatch({
          type: CATEGORY_TYPE_ITEMS_GET,
          payload: payload
        });
      });

      dispatched = true;
    }

  }
}
