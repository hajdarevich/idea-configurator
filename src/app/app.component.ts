import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { AuthenticationService } from './services/auth.service';
import { LOGOUT } from './store/auth/auth.actions';
import { IAppState } from './store';
import { categories } from './app.config';
import {
  CATEGORY_BRAND_ITEMS_GET, CATEGORY_TYPE_ITEMS_GET,
  FAN_BRAND_ITEMS_GET
} from './store/configurator/configurator.actions';


@Component({
  selector: 'app-root',
  template: `
    <app-header [visible]="authenticationService.isAuthenticated()" (onLogout)="logout()"></app-header>
    <router-outlet></router-outlet>`,
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  // variable that holds information is user logged in or not
  auth$: Observable<{}>;

  constructor(
      private store: Store<IAppState>,
      public authenticationService: AuthenticationService,
      private router: Router) {

    // @TODO find way to use dispatcher module for this
    categories.forEach((category: string) => {

      const payload: { category: string } = { category: category };

      this.store.dispatch({
        type: CATEGORY_BRAND_ITEMS_GET,
        payload: payload
      });
    });
    this.store.dispatch({
      type: FAN_BRAND_ITEMS_GET
    });
  }

  // logout from application
  logout(): void {

    this.authenticationService.logout();

    this.store.dispatch({
      type: LOGOUT
    });

    this.router.navigate(['/login'])
        .then(() => {
          location.reload();
        });
  }
}
