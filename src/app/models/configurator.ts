export interface ITableData {
  circuit: number;
  circuitId?: number;
  model: string;
  quantity: number;
  fans?: {
    value?: string;
    quantity?: number;
  };
  _id?: number;
}

export interface ICategory {
  brand: string;
  type: string;
  list: string;
  tableData: Array<ITableData>;
}

export interface IConfiguration {
  unitType: string;
  circuitNumber: string;
  refrigerant: string;
  compressor: ICategory;
  evaporator: ICategory;
  condenser: ICategory;
  heatRecovery: ICategory;
  desuperheater: ICategory;
  generalData: {
    te: string;
    tc: string;
    dtSurr: string;
    dtSub: string;
    capacity: string;
  };
}
