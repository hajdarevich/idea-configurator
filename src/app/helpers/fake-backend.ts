import { Http, BaseRequestOptions, Response, ResponseOptions, RequestMethod } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

const listItemsGetRegex: RegExp = new RegExp('/api/category/[^\n]+/brand/[^\n]+/type/[^\n]+');
const brandItemsGetRegex: RegExp = new RegExp('/api/category/[^\n]+/brand');
const typeItemsGetRegex: RegExp = new RegExp('/api/category/[^\n]+/type');

export function mockBackEndFactory(backend: MockBackend, options: BaseRequestOptions) {
                                   // use fake backend in place of Http service for backend-less development

        // configure fake backend
        backend.connections.subscribe((connection: MockConnection) => {
            const testUser = { username: 'test', password: 'test', firstName: 'Test', lastName: 'User' };

            // wrap in timeout to simulate server api call
            setTimeout(() => {

                // fake authenticate api end point
                if (connection.request.url.endsWith('/api/authenticate') && connection.request.method === RequestMethod.Post) {
                    // get parameters from post request
                    const params = JSON.parse(connection.request.getBody());

                    // check user credentials and return fake jwt token if valid
                    if (params.username === testUser.username && params.password === testUser.password) {
                        connection.mockRespond(new Response(
                            new ResponseOptions({ status: 200, body: { token: 'fake-jwt-token' } })
                        ));
                    } else {
                        connection.mockRespond(new Response(
                            new ResponseOptions({ status: 200 })
                        ));
                    }
                    return;
                }

                // fake users api end point
                if (connection.request.url.endsWith('/api/users') && connection.request.method === RequestMethod.Get) {
                    // check for fake auth token in header and return test users if valid, this security is implemented server side
                    // in a real application
                    if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                        connection.mockRespond(new Response(
                            new ResponseOptions({ status: 200, body: [testUser] })
                        ));
                    } else {
                        // return 401 not authorised if token is null or invalid
                        connection.mockRespond(new Response(
                            new ResponseOptions({ status: 401 })
                        ));
                    }
                    return;
                }

                // get list items for provided category name, brand and type
                if (listItemsGetRegex.test(connection.request.url) && connection.request.method === RequestMethod.Get) {

                    const urlParts: string[] = connection.request.url.split('/');
                    const [category, brand, type]: [string, string, string] = [urlParts[3], urlParts[5], urlParts[7]];
                    switch (category) {
                      case 'compressor':
                        connection.mockRespond(new Response(
                          new ResponseOptions({ status: 200, body: generateGetCategoryResponse('Compressor') })
                        ));
                        break;
                      case 'evaporator':
                        connection.mockRespond(new Response(
                          new ResponseOptions({ status: 200, body: generateGetCategoryResponse('Evaporator') })
                        ));
                        break;
                      case 'fans':
                        connection.mockRespond(new Response(
                          new ResponseOptions({ status: 200, body: generateGetCategoryResponse('Fan') })
                        ));
                        break;
                      case 'desuperheater':
                        connection.mockRespond(new Response(
                          new ResponseOptions({ status: 200, body: generateGetCategoryResponse('Desuperheater') })
                        ));
                        break;
                      case 'heatRecovery':
                        connection.mockRespond(new Response(
                          new ResponseOptions({ status: 200, body: generateGetCategoryResponse('Heat recovery') })
                        ));
                        break;
                      case 'condenser':
                        connection.mockRespond(new Response(
                          new ResponseOptions({ status: 200, body: generateGetCategoryResponse('Condenser') })
                        ));
                        break;
                      default:
                        connection.mockRespond(new Response(
                              new ResponseOptions({ status: 400, body: {
                                error: 'NO_CATEGORY'
                              }
                          })
                        ));
                        break;
                    }
                    return;
                }

                if (brandItemsGetRegex.test(connection.request.url) && connection.request.method === RequestMethod.Get) {

                    const urlParts: string[] = connection.request.url.split('/');
                    const [category]: [string] = [urlParts[3]];

                    switch(category) {
                      case 'compressor':
                        connection.mockRespond(new Response(
                          new ResponseOptions({ status: 200, body: generateGetCategoryResponse('Compressor brand') })
                        ));
                        break;
                      case 'evaporator':
                        connection.mockRespond(new Response(
                          new ResponseOptions({ status: 200, body: generateGetCategoryResponse('Evaporator brand') })
                        ));
                        break;
                      case 'fans':
                        connection.mockRespond(new Response(
                          new ResponseOptions({ status: 200, body: generateGetCategoryResponse('Fans brand') })
                        ));
                        break;
                      case 'desuperheater':
                        connection.mockRespond(new Response(
                          new ResponseOptions({ status: 200, body: generateGetCategoryResponse('Desuperheater brand') })
                        ));
                        break;
                      case 'heatRecovery':
                        connection.mockRespond(new Response(
                          new ResponseOptions({ status: 200, body: generateGetCategoryResponse('Heat recovery brand') })
                        ));
                        break;
                      case 'condenser':
                        connection.mockRespond(new Response(
                          new ResponseOptions({ status: 200, body: generateGetCategoryResponse('Condenser brand') })
                        ));
                        break;
                      default:
                        connection.mockRespond(new Response(
                              new ResponseOptions({ status: 400, body: {
                                error: 'NO_CATEGORY'
                              }
                          })
                        ));
                        break;
                    }
                    return;
                }

                if (typeItemsGetRegex.test(connection.request.url) && connection.request.method === RequestMethod.Get) {
                    const urlParts: string[] = connection.request.url.split('/');
                    const [category]: [string] = [urlParts[3]];

                    switch (category) {
                        case 'compressor':
                            connection.mockRespond(new Response(
                            new ResponseOptions({ status: 200, body: [
                                { value : 'Compressor type 1', img : '/assets/dragdrop2.png'} ,
                                { value : 'Compressor type 2', img : '/assets/draganddrop3.jpg'}
                            ]
                            })
                        ));
                            break;
                        case 'evaporator':
                            connection.mockRespond(new Response(
                            new ResponseOptions({ status: 200, body: [
                                { value : 'Evaporator type 1', img : '/assets/dragdrop2.png'} ,
                                { value : 'Evaporator type 2', img : '/assets/draganddrop3.jpg'}
                            ]
                            })
                        ));
                            break;
                      case 'fans':
                          connection.mockRespond(new Response(
                            new ResponseOptions({ status: 200, body: [
                              { value : 'Fan type 1', img : '/assets/dragdrop2.png'} ,
                              { value : 'Fan type 2', img : '/assets/draganddrop3.jpg'}
                            ]
                            })
                          ));
                            break;
                        case 'desuperheater':
                            connection.mockRespond(new Response(
                            new ResponseOptions({ status: 200, body: [
                                { value : 'Desuperheater type 1', img : '/assets/dragdrop2.png'} ,
                                { value : 'Desuperheater type 2', img : '/assets/draganddrop3.jpg'}
                            ]
                            })
                        ));
                            break;
                        case 'heatRecovery':
                            connection.mockRespond(new Response(
                            new ResponseOptions({ status: 200, body: [
                                { value : 'Heat recovery type 1', img : '/assets/dragdrop2.png'} ,
                                { value : 'Heat recovery type 2', img : '/assets/draganddrop3.jpg'}
                            ]
                            })
                        ));
                            break;
                        case 'condenser':
                            connection.mockRespond(new Response(
                            new ResponseOptions({ status: 200, body: [
                                { value : 'Condenser type 1', img : '/assets/dragdrop2.png'} ,
                                { value : 'Condenser type 2', img : '/assets/draganddrop3.jpg'}
                            ]
                            })
                        ));
                            break;
                        default:
                            break;

                    }
                    return;
                }
            }, 500);

        });

        return new Http(backend, options);

}

export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: Http,
    deps: [MockBackend, BaseRequestOptions],
    useFactory: mockBackEndFactory
};

function generateGetCategoryResponse(category: string): string[] {
  const categoryItems: string[] = [];
  for (let i = 0; i < generateRandomNumber(); i++) {
    categoryItems.push(category + ' ' + (i + 1).toString());
  }
  if (categoryItems.length === 0) {
    categoryItems.push(category + ' 1');
  }


  return categoryItems;
}

function generateRandomNumber(max: number = 50): number {

  return Math.floor(Math.random() * 50);
}
