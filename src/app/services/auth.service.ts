import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
    public token: string;

    /**
     * Token status reactive variable.
     *
     * @type {EventEmitter<boolean>}
     */
    public authStatus: Subject<boolean> = new Subject<boolean>();

    constructor(private http: Http) {
        // set token if saved in local storage
        const currentUser = JSON.parse(localStorage.getItem('jwt'));
        this.token = currentUser && currentUser.token;
    }

    login(username: string, password: string): Observable<boolean> {
        return this.http.post('/api/authenticate', JSON.stringify({ username: username, password: password }))
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                const token = response.json() && response.json().token;
                if (token) {
                    // set token property
                    this.token = token;

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('jwt', JSON.stringify({ username: username, token: token }));

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('jwt');
    }

    /**
     * Check if token is expired or exist and update auth status.
     *
     * @returns {boolean}
     */
    isAuthenticated(): boolean {

        const token: string = this.getToken();

        if (!token) {
            this.authStatus.next(false);
            return false;
        }

        if (token) {
            this.authStatus.next(true);
            return true;
        }
    }

    /**
     * Retrieve token from localStorage
     *
     * @returns {any}
     */
    getToken(): string {
        return localStorage.getItem('jwt');
    }
}
