import { Routes, RouterModule } from '@angular/router';


import { AuthGuard } from './services/guards/auth.guard';
import { HomeComponent } from './modules/home/home.component';
import { LoginComponent } from './_syscomponents/login.component';


const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
