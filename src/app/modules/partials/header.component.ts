import { Component, EventEmitter, Output, Input } from '@angular/core';

@Component({
    selector: 'app-header',
    styles: [`
      .logo {
        margin: 3px 15px 3px 3px;
        width: 100px;
      }
    `],
    templateUrl: 'header.component.html'
})

export class HeaderComponent {

    @Input() visible: boolean;

    @Output() onLogout: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor() { }

    /**
     * Logout from Application
     *
     */
    logout(logoutButton: HTMLButtonElement): void {
        this.onLogout.emit(true);
    }
}
