import { Component, ViewEncapsulation, ViewChildren, QueryList, ElementRef, TemplateRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { IOption } from 'ng-select';
import { DragulaService } from 'ng2-dragula';
import { el } from '@angular/platform-browser/testing/src/browser_util';

import { IAppState } from '../../store';
import {
  UNIT_TYPES_GET, REFRIGERANTS_GET, CIRCUITS_GET, CATEGORY_LIST_ITEMS_GET,
  CATEGORY_BRAND_ITEMS_GET, CATEGORY_TYPE_ITEMS_GET, FAN_LIST_ITEMS_GET, FAN_TYPE_ITEMS_GET, FAN_BRAND_ITEMS_GET
} from '../../store/configurator/configurator.actions';
import { IConfiguration, ICategory, ITableData } from '../../models/configurator';
import { defaultConfiguration, categories } from '../../app.config';
import { CategoryPickerComponent } from '../shared/category-picker/category-picker.component';
import { AlertService } from '../../services/alert.service';

@Component({
    templateUrl: 'home.component.html',
    encapsulation: ViewEncapsulation.None,
    styles: [`
      td.disabled {
        pointer-events: none;
        opacity: 0.5;
      }
      .is-disabled-categories {
        opacity: 0.50;
      }
      .table-head>tr>th {
        transition: ease .3s;
        cursor: pointer;
      }
      .table-head>tr>th:hover {
        background: #03A9F4;
        color: white;
      }
    `]
})
export class HomeComponent {

  public modalRef: BsModalRef;
  public hideCategories = {
    heatRecovery: false,
    desuperheater: false,
  };

  configurator$: Observable<{}>;

    configuration: IConfiguration = defaultConfiguration;
    circuitRange: Array<{ rowSpanIndex?: number, rowIndex?: number }> = [];
    // @TODO improve this... not the best solution for quantity management
    tempConfiguration = {
      'compressor': [],
      'evaporator': [],
      'condenser': [],
      'desuperheater': [],
      'heatRecovery': [],
    };
    errorMessage = '';
    uniqueId = 1;

    @ViewChildren('compressorModels') compressorModels: QueryList<ElementRef>;
    @ViewChildren('evaporatorModels') evaporatorModels: QueryList<ElementRef>;
    @ViewChildren('heatRecoveryModels') heatRecoveryModels: QueryList<ElementRef>;
    @ViewChildren('desuperheaterModels') desuperheaterModels: QueryList<ElementRef>;
    @ViewChildren('condenserModels') condenserModels: QueryList<ElementRef>;
    @ViewChildren('condenserFansModels') condenserFansModels: QueryList<ElementRef>;
    @ViewChildren('evaporatorFansModels') evaporatorFansModels: QueryList<ElementRef>;
    @ViewChildren(CategoryPickerComponent) categoryPickers: QueryList<CategoryPickerComponent>;

    constructor(private store: Store<IAppState>,
                private dragulaService: DragulaService,
                private alertService: AlertService,
                private modalService: BsModalService) {

      this.configurator$ = this.store.select('configurator');

      this.store.dispatch({
        type: UNIT_TYPES_GET
      });

      this.store.dispatch({
        type: REFRIGERANTS_GET
      });

      this.store.dispatch({
        type: CIRCUITS_GET
      });

      [...categories, 'fans'].forEach((category: string) => this.dragulaService.setOptions(category + '-bag', {
        copy: true,
        revertOnSpill: true,
        // this methods prevents dragging from the table, or dragging elements when
        // not all category properties are set
        invalid: (element: HTMLElement) => {
          const hasAllProperties = element.dataset.hasOwnProperty('list')
            && element.dataset.hasOwnProperty('brand') && element.dataset.hasOwnProperty('type')
            && element.dataset['list'] !== '';
          return element.parentElement.tagName.toLowerCase() === 'td' || !hasAllProperties;
        },
      }));

        // call onDrop after drag end and replace icon with model text
        dragulaService.drop.subscribe((value) => {
            this.onDrop(value);
        });

    }

    /**
     * On selected circuit update state and change table rows.
     * @param option
     */
    onSelectedCircuits(option: IOption): void {
        this.circuitRange = [...this.createRange(+option.value)];
        this.onConfigurationChanged();
    }

    /**
     * Replace dropped icon with model name from list dropdown.
     * @param args
     */
    private onDrop(args) {

        const [el, source, target] = args;

        const categoryName  = target.getAttribute('data-categoryName');
        const circuitNumber = target.getAttribute('data-circuitnumber');
        const indexInCircuit = target.getAttribute('data-indexInCircuit');
        const circuitId = target.getAttribute('data-circuitId');
        const model = source.getAttribute('data-list');

        if (categoryName === 'evaporatorFans' || categoryName === 'condenserFans') {

          // disable dropping of fans when there is no item in the corresponding table row
          if ((categoryName === 'evaporatorFans' && !this.configuration.evaporator.tableData
              .filter((item: ITableData) => item.circuitId === circuitId).length) ||
            (categoryName === 'condenserFans' && !this.configuration.condenser.tableData
              .filter((item: ITableData) => item.circuitId === circuitId).length)) {
            this.dragulaService.find('fans-bag').drake.cancel(true);
            return;
          }
          const realCategory = categoryName === 'evaporatorFans' ? 'evaporator' : 'condenser';
          const isFanAlreadyAdded = this.tempConfiguration[realCategory][+circuitNumber - 1][+indexInCircuit].fans !== undefined;
          debugger;
          if (target.children.length < 2 && !isFanAlreadyAdded) {

            this.tempConfiguration[realCategory][+circuitNumber - 1][+indexInCircuit].fans = {
              value: 'Fan 1',
              quantity: '1'
            };
            this.updateFans(realCategory, +circuitNumber, +circuitId, +indexInCircuit);
            source.innerHTML = source.getAttribute('data-list');
            source.className = '';
            return;
          } else {
            source.remove();
            return;
          }
        }


        // update object and add to cell if there is no added elements in opposite
        // don't update object and remove dragged element
        const isModelAlreadyAdded = this.configuration[categoryName].tableData
          .filter((item: ITableData) => item.circuit === circuitNumber)
          .filter((item: ITableData) => item.model === model).length > 0;
        if (target.children.length < 2 && !isModelAlreadyAdded) {

            this.configuration[categoryName].tableData.push({
                model: model,
                quantity: '1',
                circuit: circuitNumber,
                circuitId: circuitId,
                _id: this.uniqueId++
            });
            this.tempConfiguration[categoryName][+circuitNumber - 1][+indexInCircuit].quantity = '1';
        } else {
            source.remove();
        }
        console.log(this.tempConfiguration);
        source.innerHTML = source.getAttribute('data-list');
        source.className = '';
    }


    /**
     * Create range for selected circuits number to make dynamically table rows.
     * Multiply argument of function by two because for each circuit we create two rows
     * rowSpanIndex is index that is used to show circuits ( 1, 2, 3, 4 )
     * rowIndex index for each row. If selected circuits is 4 we will have eight rows
     * so rowIndex will get values ( 1, 2, 3, ... ,8 ).
     * @param number
     * @returns {number[]}
     */
    createRange(number: number): Array<{ rowSpanIndex?: number, rowIndex?: number, rowInRowIndex?: number }> {

        const items: Array<{ rowSpanIndex?: number, rowIndex?: number, rowInRowIndex?: number }> = [];
        for (let i = 1, j = 1; i <= number * 2; i++, j++) {
            if (i % 2) {
                items.push({ rowIndex: i, rowSpanIndex: j });
            } else {
                j--; // decrease by one to get for each circuits ordinary numbers 1, 2, 3, 4
                items.push({ rowIndex: i, rowSpanIndex: j });
            }
        }
        // this loop adds alternating index(0, 1 - starting from zero) for each item
        // it is used later to operate on the array to remove items
        items.forEach((item: { rowSpanIndex?: number, rowIndex?: number, rowInRowIndex?: number }, index: number) => {
          item['rowInRowIndex'] = index % 2;
          item['_id'] = this.uniqueId++;
        });
        categories.forEach((category: string) => {
          this.tempConfiguration[category] = [];
          for (let i = 0; i < number; i++) {
            this.tempConfiguration[category].push([{ quantity: undefined }, { quantity: undefined }]);
          }
        });
        return items;
    }

    /**
     * Emit change in dropdown configuration
     */
    onConfigurationChanged(): void {
      if (this.configuration.unitType &&
          this.configuration.circuitNumber &&
          this.configuration.refrigerant) {
        this.clearConfiguration();
        // dispatch actions to get new brands and types, clear list
        categories.forEach((category: string) => {

          const payload: { category: string } = { category: category };

          this.store.dispatch({
            type: CATEGORY_BRAND_ITEMS_GET,
            payload: payload
          });
          this.store.dispatch({
            type: CATEGORY_TYPE_ITEMS_GET,
            payload: payload
          });
        });
        this.store.dispatch({ type: FAN_BRAND_ITEMS_GET });
      }
    }

    /**
     * This method is called when brand is selected.
     * With this method we fetch types for selected brand of each categories.
     * categoryName is equal to property in configuration object
     * @param categoryName
     * @param brand
     */
    onBrandChanged(categoryName: string, brand: string): void {

        if (!brand) {
            return;
        }

        this.configuration[categoryName].brand = brand;

        this.store.dispatch({
            type: CATEGORY_TYPE_ITEMS_GET,
            payload: {
                category: categoryName,
                brand: brand,
            }
        });
    }

    /**
     * Called when brand is changed on fan picker
     * @param {string} brand
     */
    onFanBrandChanged(brand: string): void {

      if (!brand) {
        return;
      }

      this.store.dispatch({
        type: FAN_TYPE_ITEMS_GET,
        payload: {
          brand: brand
        }
      });
    }

    /**
     * This method is called when one of the dropdowns in the sidebar changes
     * categoryName is equal to property in configuration object
     * @param categoryName
     * @param category
     */
    onCategoryChanged(categoryName: string, category: ICategory): void {

      if (!category) {
        return;
      }

      this.configuration[categoryName].type = category.type;

      this.store.dispatch({
        type: CATEGORY_LIST_ITEMS_GET,
        payload: {
          category: categoryName,
          brand: category.brand,
          type: category.type
        }
      });
      console.log(this.configuration);
    }

    /**
     * Called when fan parameters are changed
     * @param {ICategory} category
     */
    onFanCategoryChanged(category: ICategory): void {

      if (!category) {
        return;
      }

      this.store.dispatch({
        type: FAN_LIST_ITEMS_GET,
        payload: {
          brand: category.brand,
          type: category.type
        }
      });
    }

    /**
     * Called when a list item is selected from the dropdown, updates configuration
     * @param categoryName
     * @param listItem
     */
    onListChanged(categoryName: string, listItem: string): void {

      this.configuration[categoryName].list = listItem;
    }

  /**
   * Called when a delete icon is clicked
   * @param modelData
   * @param category
   */
    onDeleteClicked(modelData: { rowSpanIndex?: number, rowIndex?: number, rowInRowIndex?: number, _id?: number }, category: string): void {

      // rowSpanIndex is equal to circuit number, rowInRowIndex is equal to 0 or 1 depending on which item was clicked in the row
      this.configuration[category].tableData = this.configuration[category]
        .tableData
        .filter((item: ITableData) => !(+item.circuit === modelData.rowSpanIndex && +item.circuitId === modelData._id));
      // we now have to delete the corresponding element
      let modelCells: ElementRef[] = this.getModelCells(category);
      // reduce to only two items
      modelCells = modelCells.filter((elem: ElementRef, elemIndex: number) => elemIndex >= (modelData.rowSpanIndex - 1) * 2
        && elemIndex < modelData.rowSpanIndex * 2);
      // 0 is hardcoded because only one element can be present at a given time
      modelCells[modelData.rowInRowIndex].nativeElement.children[0].remove();
      // reset quantity in temporary configuration
      this.tempConfiguration[category][modelData.rowSpanIndex - 1][modelData.rowInRowIndex].quantity = undefined;
      // reset fans quantity for some categories
      if (category.toLowerCase() === 'condenser' || category.toLowerCase() === 'evaporator') {
        delete this.tempConfiguration[category][modelData.rowSpanIndex - 1][modelData.rowInRowIndex].fans;
        // also clear fans
        let fansModelCells: ElementRef[] =
          this.getModelCells(category.toLowerCase() === 'condenser' ? 'condenserFans' : 'evaporatorFans');
        fansModelCells = fansModelCells.filter(((elem: ElementRef, elemIndex: number) => elemIndex >= (modelData.rowSpanIndex - 1) * 2
          && elemIndex < modelData.rowSpanIndex * 2));
        if (fansModelCells[modelData.rowInRowIndex].nativeElement.children.length) {
          fansModelCells[modelData.rowInRowIndex].nativeElement.children[0].remove();
        }
      }
    }

  /**
   * This method resets the component to the initial state
   * It is called after any of the main parameters are changed
   */
  clearConfiguration(): void {

    this.configuration = Object.assign({}, this.configuration, {
      compressor:  { brand: '', type: '', list: '', tableData: [] },
      evaporator: { brand: '', type: '', list: '', tableData: [] },
      condenser: { brand: '', type: '', list: '', tableData: [] },
      heatRecovery: { brand: '', type: '', list: '', tableData: [] },
      desuperheater: { brand: '', type: '', list: '', tableData: [] }
    });
    [...categories, 'condenserFans', 'evaporatorFans'].forEach((category: string) => {
      this.getModelCells(category).forEach((element: ElementRef) => {
        if (element.nativeElement.children.length > 0) {
          element.nativeElement.children[0].remove();
        }
      });
    });
    // reset category pickers
    this.categoryPickers.forEach((categoryPicker: CategoryPickerComponent) => categoryPicker.reset());
    // clear quantity trackers
    categories.forEach((category: string) => {
      this.tempConfiguration[category].forEach((categoryItem: Array<{ quantity: number, fans?: {
        quantity: number, value: string
      }}>) => {
        categoryItem.forEach((item: { quantity: number, fans?: {
          quantity: number, value: string
        }}) => {
          item.quantity = undefined;
          if (item.hasOwnProperty('fans')) {
            delete item.fans;
          }
        })
      });
    });
  }

  /**
   * This method updates the quantity of a model for a given circuit
   * and category
   * @param category
   * @param circuitNumber
   * @param circuitId
   * @param selectItem
   */
  updateQuantity(category: string, circuitNumber: number, circuitId: number, selectItem: { name: string, value: string }): void {

    this.configuration[category].tableData
      .filter((item: ITableData) => +item.circuit === circuitNumber)
      .forEach((item: ITableData) => {
        if (+item.circuitId === circuitId) {
          item.quantity = +selectItem.value;
        }
      });
  }

  /**
   * This method updates the fans when the quantity or value is changed
   * @param {string} category
   * @param {number} circuitNumber
   * @param {number} circuitId
   * @param {number} rowInRowIndex
   */
  updateFans(category: string, circuitNumber: number, circuitId: number, rowInRowIndex: number): void {

    this.configuration[category].tableData
      .filter((item: ITableData) => +item.circuit === circuitNumber)
      .forEach((item: ITableData) => {
        if (+item.circuitId === circuitId) {

            Object.assign(item, {
              fans: Object.assign({}, this.tempConfiguration[category][circuitNumber - 1][rowInRowIndex].fans)
            });
        }
      });
  }

  /**
   * This method is called when submit is clicked
   * Validates current configuration and outputs an appropriate message
   */
  submitConfiguration(): void {

    console.log(this.configuration);

    const isValidConfiguration = this.isConfigurationValid();
    if (!isValidConfiguration.status) {
      this.errorMessage = isValidConfiguration.message;
      return;
    }
    this.errorMessage = '';
  }

  /**
   * This method resets general data section of configuration
   */
  clearGeneralData(): void {

    this.configuration.generalData.capacity = '';
    this.configuration.generalData.dtSub = '';
    this.configuration.generalData.dtSurr = '';
    this.configuration.generalData.te = '';
    this.configuration.generalData.tc = '';
  }

  /**
   * Utility method to get the corresponding cells in the table
   * @param category
   * @returns {any}
   */
  private getModelCells(category: string): ElementRef[] {
    switch (category) {
      case 'compressor':
        return this.compressorModels.map((el: ElementRef) => el);
      case 'evaporator':
        return this.evaporatorModels.map((el: ElementRef) => el);
      case 'evaporatorFans':
        return this.evaporatorFansModels.map((el: ElementRef) => el);
      case 'desuperheater':
       return this.desuperheaterModels.map((el: ElementRef) => el);
      case 'condenser':
        return this.condenserModels.map((el: ElementRef) => el);
      case 'condenserFans':
        return this.condenserFansModels.map((el: ElementRef) => el);
      case 'heatRecovery':
       return this.heatRecoveryModels.map((el: ElementRef) => el);
      default:
        return [];
    }
  }

  /**
   * Method to validate current configuration
   * @return {boolean}
   */
  private isConfigurationValid(): { status: boolean, message?: string } {

    // check main parameters
    if (!(this.configuration.unitType && this.configuration.circuitNumber && this.configuration.refrigerant)) {
      return {
        status: false,
        message: 'Not all main parameters are set!'
      };
    }

    // check if table data is set
    let hasSomeTableData = false;
    for (let i = 0; i < categories.length; i++) {
      if (this.configuration[categories[i]].tableData.length > 0) {
        hasSomeTableData = true;
        break;
      }
    }

    if (!hasSomeTableData) {
      return {
        status: false,
        message: 'The table is empty!'
      };
    }

    // verify other parameters -> quantity, fans
    categories.forEach((category: string) => {
      if (this.configuration[category].tableData
          .some((item: ITableData) => !item.quantity)) {
        return {
          status: false,
          message: 'Quantity cannot be zero or undefined!'
        };
      }
    });

    // verify based on unit type
    switch (this.configuration.unitType.toUpperCase()) {

      case 'AA': {
        // fans are required for both evaporator and condenser
        if (this.configuration['evaporator'].tableData
            .some((item: ITableData) => !item.hasOwnProperty('fans') || !item.fans.quantity || !item.fans.value)) {
          return {
            status: false,
            message: 'Fans for evaporators were not selected or their quantity is zero!'
          };
        }
        if (this.configuration['condenser'].tableData
            .some((item: ITableData) => !item.hasOwnProperty('fans') || !item.fans.quantity || !item.fans.value)) {
          return {
            status: false,
            message: 'Fans for condensers were not selected or their quantity is zero!'
          };
        }
        break;
      }
      case 'WA': {
        if (this.configuration['evaporator'].tableData
            .some((item: ITableData) => !item.hasOwnProperty('fans') || !item.fans.quantity || !item.fans.value)) {
          return {
            status: false,
            message: 'Fans for evaporators were not selected or their quantity is zero!'
          };
        }
        break;
      }
      case 'AW': {
        if (this.configuration['condenser'].tableData
            .some((item: ITableData) => !item.hasOwnProperty('fans') || !item.fans.quantity || !item.fans.value)) {
          return {
            status: false,
            message: 'Fans for condensers were not selected or their quantity is zero!'
          };
        }
        break;
      }
    }

    return {
      status: true
    };
  }

  public openGeneralDataModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  public openSettingsModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
}
