import { NgModule, ModuleWithProviders, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SelectModule } from 'ng-select';
import { DragulaModule } from 'ng2-dragula';

import { PanelComponent } from './panel/panel.component';
import { CategoryPickerComponent } from './category-picker/category-picker.component';
import { ArrayPipesModule } from '../../pipes/array-pipes.module';
import { TableConfiguratorComponent } from './table-configurator/table.component';
import { TableBodyConfiguratorComponent } from './table-configurator/table-body.component';
import { SpinnerComponent } from './spinner/spinner.component';

@NgModule({
  declarations: [
    TableConfiguratorComponent,
    TableBodyConfiguratorComponent,
    CategoryPickerComponent,
    PanelComponent,
    SpinnerComponent
  ],
  imports: [
      ReactiveFormsModule,
      DragulaModule,
      ArrayPipesModule,
      SelectModule,
      CommonModule,
      FormsModule
  ],
  exports: [
    TableConfiguratorComponent,
    TableBodyConfiguratorComponent,
    CategoryPickerComponent,
    PanelComponent,
    SpinnerComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class SharedModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule
    };
  }
}
