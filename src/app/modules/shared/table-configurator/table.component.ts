import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-table-configurator',
    templateUrl: 'table.component.html',
})
export class TableConfiguratorComponent {

    @Input() circuits: string;
}
