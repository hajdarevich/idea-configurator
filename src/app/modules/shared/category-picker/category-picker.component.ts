import { Component, ChangeDetectionStrategy, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';

import { ICategory } from '../../../models/configurator';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-category-picker',
  styles: [`
    :host {
      display: flex;
      flex-direction: column;
    }
    .icon-large { font-size: 2em; }
    .center {
      display: table-cell;
      vertical-align: middle;
    }
    .main {
      display: table;
    }
    .dnd-area {
      display: flex;
      align-items: center;
      justify-content: center;
      width: 100%;
      height: 50px;
      margin-top: 10px;
      background: #cdcdcd;
      cursor: pointer;
      transition: ease .3s;
    }
    .dnd-area:before {
      content: "Drag this!";
    }
    .dnd-area:hover {
      background: #9c9c9c;
    }
    .loading {
      position: absolute;
      top: 5px;
      right: 10px;
    }
  `],
  template: `
  <div class="">
    <div class="loading" *ngIf="loading"><app-spinner></app-spinner></div>
    <div class="col-lg-12 col-md-12 col-sm-12" [formGroup]="categoryForm">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <label class="control-label">Brand:</label>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12">
          <ng-select
                  formControlName="brand"
                  placeholder="Choose brand..."
                  [disabled]="disabled || loading"
                  [options]="brands | beLabelValueDropdown"
                  (selected)="onBrandChanged.emit($event.value)">
          </ng-select>
      </div>
    </div>
    <div [formGroup]="categoryForm" class="col-lg-12 col-md-12 col-sm-12 app-row-spacing">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <label class="control-label">Type:</label>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12">
          <ng-select
                  formControlName="type"
                  placeholder="Choose type..."
                  [disabled]="disabled || loading || !categoryForm.value.brand"
                  [options]="types"
                  (selected)="onTypeChangedUpdateImage($event); onCategoryChanged.emit(categoryForm.value)">
                    <ng-template
                      #optionTemplate
                      let-option="option">
                      <span>{{ option.value  }}</span>
                    </ng-template>
          </ng-select>
      </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <label class="control-label">List:</label>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12">
          <ng-select
                  placeholder="Choose list item..."
                  [(ngModel)]="listItem"
                  [disabled]="disabled || !categoryForm.value.brand || !categoryForm.value.type || loading"
                  [options]="listItems | beLabelValueDropdown"
                  (selected)="onListChanged.emit($event.value)">
          </ng-select>
      </div>
    </div>
  </div>
  <div class="text-center" [dragula]="bag">
   <span class="dnd-area"
         [attr.data-brand]="categoryForm.get('brand').value"
         [attr.data-type]="categoryForm.get('type').value"
         [attr.data-list]="listItem"
         aria-hidden="true">
           <img src="{{ dragAndDropImage }}"
                height="35"
                width="35"
                [attr.data-brand]="categoryForm.get('brand').value"
                [attr.data-type]="categoryForm.get('type').value"
                [attr.data-list]="listItem"
                aria-hidden="true">
</span>
  </div>
`
})
export class CategoryPickerComponent {

  categoryForm: FormGroup;
  listItem = '';
  dragAndDropImage = '/assets/dragdrop1.png';

  @Input() disabled = false;
  @Input() loading = false;
  @Input() listItems: string[] = [];
  @Input() brands: string[] = [];
  @Input() types: string[] = [];
  @Input() bag = '';

  @Output() onCategoryChanged: EventEmitter<ICategory> = new EventEmitter<ICategory>();
  @Output() onBrandChanged: EventEmitter<string> = new EventEmitter<string>();
  @Output() onListChanged: EventEmitter<string> = new EventEmitter<string>();

  constructor(private fb: FormBuilder) {
    this.categoryForm = this.fb.group({
      brand: new FormControl(undefined),
      type: new FormControl(undefined)
    });

    // don't emit onCategoryChanged event that will update list of models if brand and types are not selected and don't have values
    this.categoryForm
      .valueChanges
      .filter((categoryValue: ICategory) => !!(categoryValue.brand && categoryValue.type))
      .subscribe((categoryValue: ICategory) => this.onCategoryChanged.emit(categoryValue));
  }

  /**
   * This method is called when brand is selected.
   * With this method we fetch types for selected brand of each categories.
   * categoryName is equal to property in configuration object
   * @param event
   */
  onTypeChangedUpdateImage(event: { img: string }): void {

    if (!event) {
      return;
    }

    this.dragAndDropImage = event.img;

  }

  /**
   * Resets the component configuration to base state
   */
  reset(): void {
    this.categoryForm.reset();
    this.listItem = '';
    this.dragAndDropImage = '/assets/dragdrop1.png';
  }
}
