import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-panel',
  styles: [`
    :host {
      position: relative;
      display: flex;
      flex-direction: row;
      width: calc(100% / 5 - 20px);
      float: left;
      margin: 0 10px;
    }
    @media (max-width: 1024px) {
      :host {
        width: calc(100% / 3 - 20px);
      }
    }
    @media (max-width: 800px) {
      :host {
        width: 100%;
      }
    }

    .title {
      width: fit-content;
      border-bottom-right-radius: 4px;
      height: 30px;
      background: #03A9F4;
      color: white;
      padding: 5px 10px;
    }
    .content {
      height: auto;
      margin-top: 15px;
    }
  `],
  template: `
    <div class="panel panel-{{_class}}">
      <div>
        <div class="title">
          <b>{{title}}</b>
        </div>
        <div class="content">
          <ng-content select="panel-body"></ng-content>
        </div>
      </div>
    </div>

  <!--<div class="panel panel-{{_class}}">-->
    <!--<div class="btn-group pull-right">-->
      <!--<ng-content select="panel-toolbar"></ng-content>-->
    <!--</div>-->
    <!--<div class="panel-body">-->
        <!--<p class="pull-left col-lg-12 col-md-12 col-sm-12"><b>{{title}}</b></p>-->
        <!--<ng-content select="panel-body"></ng-content>-->
    <!--</div>-->
  <!--</div>-->
`
})
export class PanelComponent {

  @Input('class') _class = '';
  @Input() title = '';
}
