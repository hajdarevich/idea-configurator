import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/auth.service';
import { Store } from '@ngrx/store';
import { LOGIN } from '../store/auth/auth.actions';
import { IAppState } from '../store/index';

@Component({
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    error = '';
    
    constructor(
        private store: Store<IAppState>,
        private router: Router,
        private authenticationService: AuthenticationService) { }
    
    ngOnInit() {
        // reset login status
        this.authenticationService.logout();
    }
    
    login() {
    
        this.loading = true;
        
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(result => {
                if (result === true) {
                    this.router.navigate(['/']);
    
                    this.store.dispatch({
                        type: LOGIN
                    });
                    
                } else {
                    this.error = 'Username or password is incorrect';
                    this.loading = false;
                }
            });
    }
}
