import { IConfiguration, ICategory } from './models/configurator';

const defaultCategoryConfiguration: ICategory = {
  brand: '',
  type: '',
  list: '',
  tableData: []
};

export const defaultConfiguration: IConfiguration = {
  unitType: undefined,
  circuitNumber: undefined,
  refrigerant: undefined,
  compressor: {     brand: '',     type: '',     list: '',     tableData: []   },
  evaporator: {     brand: '',     type: '',     list: '',     tableData: []   },
  condenser: {     brand: '',     type: '',     list: '',     tableData: []   },
  heatRecovery: {     brand: '',     type: '',     list: '',     tableData: []   },
  desuperheater: {     brand: '',     type: '',     list: '',     tableData: []   },
  generalData: {
    dtSub: '',
    tc: '',
    te: '',
    dtSurr: '',
    capacity: ''
  }
};

export const categories: string[] = [
  'compressor',
  'evaporator',
  'desuperheater',
  'heatRecovery',
  'condenser'
];
